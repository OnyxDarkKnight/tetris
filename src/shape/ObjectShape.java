package shape;

import game.GamePanel;
import utility.GlobalValues;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by siliut on 13/10/2015.
 * This is the actual Tetrimino shape
 */
public class ObjectShape {
    // This is a list containing all the pieces this shape has
    private ArrayList<Piece> pieces = new ArrayList<Piece>();
    // These are used to store the position and boundaries of this
    public int x, y, minWidth = 0, maxWidth = 0, minHeight = 0, maxHeight = 0;

    // This function removes the piece given
    public void removePiece(Piece p) {
        pieces.remove(p);
    }

    // This will remove all pieces from the shape
    public void removePieces()
    {
        pieces.removeAll(pieces);
    }

    // This function adds a piece if it can
    public void addPiece(Piece p) {
        for (Piece piece : pieces)
            if (p.getX() == piece.getX() && p.getY() == piece.getY()) {
                System.out.println("Invalid addition. There already exists a piece at location x=" + p.getX() + " and  y=" + p.getY() + " in the current shape.");
                return;
            }
        pieces.add(p);
    }

    // This will add new pieces similar to another shape provided
    public void addPieces(ObjectShape o)
    {
        for(Piece p: o.pieces) pieces.add(new Piece(p));
        setBoundingBox();
    }

    // This returns the list of pieces
    public ArrayList<Piece> getPieces() {
        return pieces;
    }

    // This ets the x position of the shape
    public ObjectShape setX(int x) {
        this.x = x;
        return this;
    }

    // This ets the y position of the shape
    public ObjectShape setY(int y) {
        this.y = y;
        return this;
    }

    // Returns true if this piece can be rotated to the left
    public boolean canRotateLeft(GamePanel game) {
        for (Piece p : pieces)
            if (!game.isEmpySpaceAt(x / GlobalValues.size - p.getY(), y / GlobalValues.size + p.getX())) return false;
        return true;
    }

    // This rotates the shape to the left and recalculates the boundaries
    public void rotateLeft() {
        for (Piece p : pieces) {
            int temp = p.getY();
            p.setY(p.getX());
            p.setX(-temp);
        }
        this.setBoundingBox();
    }

    // Returns true if this piece can be rotated to the right
    public boolean canRotateRight(GamePanel game) {
        for (Piece p : pieces)
            if (!game.isEmpySpaceAt(x / GlobalValues.size + p.getY(), y / GlobalValues.size - p.getX())) return false;
        return true;
    }

    // This rotates the shape to the right and recalculates the boundaries
    public void rotateRight() {
        for (Piece p : pieces) {
            int temp = p.getX();
            p.setX(p.getY());
            p.setY(-temp);
        }
        this.setBoundingBox();
    }

    // This draws the shape
    public void paint(Graphics g) {
        for (Piece p : pieces)
            p.paint(g, GlobalValues.infoWidth + x, y);
    }

    // THis also draws the shape, but it allows you to be more flexible with the positioning
    public void paint(Graphics g, int x, int y) {
        for (Piece p : pieces)
            p.paint(g, x, y);
    }

    // This calculates the boundaries of the shape
    public void setBoundingBox() {
        minWidth = maxWidth = minHeight = maxHeight = 0;
        for (Piece p : pieces) {
            if (p.getX() < minWidth) minWidth = p.getX();
            if (p.getX() > maxWidth) maxWidth = p.getX();
            if (p.getY() < minHeight) minHeight = p.getY();
            if (p.getY() > maxHeight) maxHeight = p.getY();
        }
    }

    // Returns the width of this
    public int getWidth()
    {
        return 1 + Math.abs(minWidth) + Math.abs(maxWidth);
    }

    // Returns the height of this
    public int getHeight()
    {
        return 1 + Math.abs(minHeight) + Math.abs(maxHeight);
    }

    // Returns true if the shape is entirely visible on the grid
    public boolean isOnScreen(GamePanel game) {
        for (Piece p : pieces)
            if (!game.isCellAt((x + p.getX() * GlobalValues.size) / GlobalValues.size, (y + p.getY() * GlobalValues.size) / GlobalValues.size))
                return false;
        return true;
    }

    // Returns true if the shape is partially visible on the grid
    public boolean isPartiallyOnScreen(GamePanel game) {
        int off = 0, on = 0;
        for (Piece p : pieces)
            if (!game.isCellAt((x + p.getX() * GlobalValues.size) / GlobalValues.size, (y + p.getY() * GlobalValues.size) / GlobalValues.size)) off++;
        else on++;
        return off > 0 && on > 0;
    }

    // Returns true if the shape is not visible on the grid
    public boolean isOffScreen(GamePanel game) {
        for (Piece p : pieces)
            if (game.isCellAt((x + p.getX() * GlobalValues.size) / GlobalValues.size, (y + p.getY() * GlobalValues.size) / GlobalValues.size))
                return false;
        return true;
    }

    // Returns the actual X position of the shape
    public int posX()
    {
        return x/GlobalValues.size;
    }

    // Returns the actual X position of the shape
    public int posY()
    {
        return y/GlobalValues.size;
    }
}
