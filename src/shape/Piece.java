package shape;

import utility.EnumPieceType;
import utility.GlobalValues;

import java.awt.*;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;

/**
 * Created by siliut on 13/10/2015.
 * This is the individual piece the tetriminos are composed of
 */
public class Piece {

    // Stores the type, rgb value
    private EnumPieceType type;
    private int rgb;

    // A few information that now are useless, but in the future won't
    private int shieldDurability;
    private int health;
    private int explosionSize;

    // The position of piece
    private int x, y;

    /**
     * 4 constructors, one to create a new piece in regards with a given one; one with just the rgb value,
     * the other with type and rgb and the last one that contains all the information
     */
    public Piece(Piece p) {
        setRGB(p.getRgb());
        setType(p.getType());
        setExplosionSize(p.getExplosionSize());
        setHealth(p.getHealth());
        setShieldDurability(p.getShieldDurability());
        setX(p.getX());
        setY(p.getY());
    }

    public Piece(int rgb) {
        setRGB(rgb);
        setType(EnumPieceType.NORMAL);
    }

    public Piece(EnumPieceType type, int rgb) {
        setType(type);
        setRGB(rgb);
    }

    public Piece(EnumPieceType type, int rgb, int shieldDurability, int health, int explosionSize) {
        this(type, rgb);
        setShieldDurability(shieldDurability);
        setHealth(health);
        setExplosionSize(explosionSize);
    }

    /**
     * From here until paint are methods used for setting and getting certains values (such as the health, type, explosion size etc)
     */
    public Piece setType(EnumPieceType type) {
        this.type = type;
        return this;
    }

    public EnumPieceType getType() {
        return type;
    }

    public int getShieldDurability() {
        return shieldDurability;
    }

    public Piece setShieldDurability(int shieldDurability) {
        this.shieldDurability = shieldDurability;
        return this;
    }

    public int getHealth() {
        return health;
    }

    public Piece setHealth(int health) {
        this.health = health;
        return this;
    }

    public Piece setExplosionSize(int explosionSize) {
        this.explosionSize = explosionSize;
        return this;
    }

    public int getExplosionSize() {
        return explosionSize;
    }

    public Piece setRGB(int rgb) {
        this.rgb = rgb;
        return this;
    }

    public int getRgb() {
        return rgb;
    }

    public Piece setX(int x) {
        this.x = x;
        return this;
    }

    public Piece setY(int y) {
        this.y = y;
        return this;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Color getColor() {
        return new Color(getRgb());
    }

    // This renders the piece; it draws an image if it can, or simply rectangles otherwise
    public void paint(Graphics g, int x, int y) {
        if(GlobalValues.piece != null) {
            Image coloredPiece = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(GlobalValues.piece.getSource(), new ColoringFilter(getColor())));
            g.drawImage(coloredPiece, x + getX() * GlobalValues.size, y + getY() * GlobalValues.size, null);
        }
        else{
            Graphics2D g2 = (Graphics2D)g;
            g.setColor(getColor());
            g.fillRect(x + getX() * GlobalValues.size, y + getY() * GlobalValues.size, GlobalValues.size, GlobalValues.size);
            g.setColor(getColor().darker());
            g2.setStroke(new BasicStroke(3));
            g.drawRect(x + getX() * GlobalValues.size, y + getY() * GlobalValues.size, GlobalValues.size, GlobalValues.size);
        }
    }
}

// This is a filter used to color the image
class ColoringFilter extends RGBImageFilter {
    private Color col;

    public ColoringFilter(Color col) {
        this.col = col;
    }

    public int filterRGB(int x, int y, int argb) {
        return (argb & col.getRGB());
    }
}
