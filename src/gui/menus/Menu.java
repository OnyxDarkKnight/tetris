package gui.menus;

import game.GamePanel;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by siliut on 05/12/2015.
 */
public class Menu {
    // Variables used to store the name of the menu (used to compare if the screen value is equals to this), an array
    // containing all menus created and initialized and a gamePanel (which some menus need)
    public String name;
    public static ArrayList<Menu> menus = new ArrayList<Menu>();
    public GamePanel game;
    public Menu(String name, GamePanel game)
    {
        this.name = name;
        this.game = game;
        menus.add(this);
    }

    public void renderScreen(Graphics g){}

    public void clickedOnScreen(MouseEvent e){}
}
