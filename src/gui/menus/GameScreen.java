package gui.menus;

import game.Cell;
import game.GamePanel;
import shape.ObjectShape;
import utility.GlobalValues;

import java.awt.*;

/**
 * Created by siliut on 05/12/2015.
 * This is the actual game screen, where all the pieces and cells are rendered
 */
public class GameScreen extends Menu
{
    public GameScreen(GamePanel game)
    {
        super("Game", game);
    }

    /*
        This function renders an outline of the game so players can see the boundaries better, draws a rectangle for the
        part containing the next terimino piece, the pause and menu button and highscore; it then renders the cells if
        that function is enabled (which is not by default), renders the currently falling shape and then the fallen ones.
        Draws a square where the next shape will be displayed and then draws that. If it is Game Over, it draws a
        rectangle saying "GAME OVER", the current highscore and displays the reset and exit buttons. Lastly draws the
        highscore in the top left corner, renders the main menu and pause buttons and if the game is paused it writes
        "GAME PAUSED" in the middle of the game screen
    */
    public void renderScreen(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g.setColor(new Color(20, 70, 167).darker().darker());
        g2.setStroke(new BasicStroke(3));
        g.drawRect(0, 0, GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size, GlobalValues.gameHeight * GlobalValues.size);
        if (game.cellsEnabled && !game.cells.isEmpty())
            for (Cell c : game.cells)
                c.paint(g);
        if (game.currentShape != null) game.currentShape.paint(g);
        g.setColor(new Color(20, 70, 167));
        g.fillRect(0, 0, GlobalValues.infoWidth, GlobalValues.gameHeight * GlobalValues.size);

        if (!game.fallenShapes.isEmpty())
            for (ObjectShape s : game.fallenShapes)
                s.paint(g);

        g.setColor(new Color(20, 70, 167).darker().darker());
        g.fillRect(GlobalValues.size - 20, GlobalValues.size + 20, 4 * GlobalValues.size + 40, 6 * GlobalValues.size);

        g2.setStroke(new BasicStroke(7));
        g.setColor(new Color(20, 70, 167).darker());
        g.drawRect(GlobalValues.size - 20, GlobalValues.size + 20, 4 * GlobalValues.size + 40, 6 * GlobalValues.size);

        if (game.nextShape != null)
            game.nextShape.paint(g, 2 * GlobalValues.size - (game.nextShape.minWidth * GlobalValues.size) / 2, 5 * GlobalValues.size - 10 - (game.nextShape.getHeight() * GlobalValues.size) / 2);
        if (game.gameOver) {
            g.setColor(Color.BLACK);
            g.fillRect(GlobalValues.infoWidth + 20, GlobalValues.gameHeight * GlobalValues.size / 2 - 100, GlobalValues.infoWidth + 240, 240);
            g2.setStroke(new BasicStroke(3));
            g.setColor(Color.gray);
            g.drawRect(GlobalValues.infoWidth + 20, GlobalValues.gameHeight * GlobalValues.size / 2 - 100, GlobalValues.infoWidth + 240, 240);
            game.drawStringWithShadow(g, "GAME OVER", 40, GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size / 2, GlobalValues.gameHeight * GlobalValues.size / 2 - 40, Color.gray, Color.white, true);
            game.drawStringWithShadow(g, String.format("%09d", game.score), 30, GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size / 2, GlobalValues.gameHeight * GlobalValues.size / 2, Color.gray, Color.white, true);
            game.reset.render(g);
            game.exit.render(g);
        }
        game.drawStringWithShadow(g, "HIGHSCORE:", 20, GlobalValues.infoWidth / 2, 20, Color.gray, Color.white, true);
        game.drawStringWithShadow(g, String.format("%09d", game.score), 20, GlobalValues.infoWidth / 2, 40, Color.gray, Color.white, true);
        game.pause.render(g);
        game.menu.render(g);
        if (game.pause.getState())
            game.drawStringWithShadow(g, "Game Paused", 40, GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size / 2, GlobalValues.gameHeight * GlobalValues.size / 2, Color.gray, Color.white, true);
    }
}
