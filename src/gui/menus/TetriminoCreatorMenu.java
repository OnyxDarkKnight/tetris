package gui.menus;

import game.Cell;
import game.GamePanel;
import gui.buttons.Button;
import gui.buttons.ButtonMainMenu;
import shape.Piece;
import utility.EnumPieceType;
import utility.GlobalValues;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by siliut on 05/12/2015.
 * This menu is the custom creator of tetriminos
 */
public class TetriminoCreatorMenu extends Menu {
    // This store a list of cells used to mark the area the player can place the pieces
    public ArrayList<Cell> cells = new ArrayList<Cell>();
    // A list that will contain all the placed pieces
    public ArrayList<Piece> pieces = new ArrayList<Piece>();
    // An example piece so the player can see how the piece will look before placing it down
    Piece example_piece = new Piece(0);
    // Variables for the color of the piece and some values of the position of the cells
    int red = 255, green = 255, blue = 255, cellPositionX=(GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size)/2 - 2*GlobalValues.size, cellPositionY=200;
    // This is used to store the type of the piece
    EnumPieceType type = EnumPieceType.NORMAL;
    // This variable is used in naming the file that will be generated, this can't be changed for now, but will be in the
    // future (this means you can't create more than one shape with the creator)
    public String name = "piece1";

    /**
     *  From here until the constructor are nothing but buttons that lead to the main menu, increase the value of each
     *  individual value of the rgb and the type of the piece and a button that will create the piece and add it to the
     *  game along with any other tetrimino file in the folder
     */
    Button menu = new ButtonMainMenu("PieceCreator").setWidth(150).setX(20).setY(20);
    //Red
    Button prevRed = new Button(){
        public void onButtonClicked()
        {
            if(red > 0) red--;
            game.repaint();
        }
    }.setWidth(15).setX(20).setY(170).setText("<").setFrameThickness(4).setScreen("PieceCreator");
    Button nextRed = new Button(){
        public void onButtonClicked()
        {
            if(red < 255) red++;
            game.repaint();
        }
    }.setWidth(15).setX(80).setY(170).setText(">").setFrameThickness(4).setScreen("PieceCreator");

    //Green
    Button prevGreen = new Button(){
        public void onButtonClicked()
        {
            if(green > 0) green--;
            game.repaint();
        }
    }.setWidth(15).setX(20).setY(240).setText("<").setFrameThickness(4).setScreen("PieceCreator");
    Button nextGreen = new Button(){
        public void onButtonClicked()
        {
            if(green < 255) green++;
            game.repaint();
        }
    }.setWidth(15).setX(80).setY(240).setText(">").setFrameThickness(4).setScreen("PieceCreator");

    //Blue
    Button prevBlue = new Button(){
        public void onButtonClicked()
        {
            if(blue > 0) blue--;
            game.repaint();
        }
    }.setWidth(15).setX(20).setY(310).setText("<").setFrameThickness(4).setScreen("PieceCreator");
    Button nextBlue = new Button(){
        public void onButtonClicked()
        {
            if(blue < 255) blue++;
            game.repaint();
        }
    }.setWidth(15).setX(80).setY(310).setText(">").setFrameThickness(4).setScreen("PieceCreator");

    //Type
    Button nextType = new Button(){
        public void onButtonClicked()
        {
            type = type.next();
            game.repaint();
        }
    }.setWidth(15).setX(80).setY(380).setText(">").setFrameThickness(4).setScreen("PieceCreator");
    Button previousType = new Button(){
        public void onButtonClicked()
        {
            type = type.previous();
            game.repaint();
        }
    }.setWidth(15).setX(20).setY(380).setText("<").setFrameThickness(4).setScreen("PieceCreator");

    //Create
    Button create = new Button(){
        public void onButtonClicked()
        {
            if(!pieces.isEmpty()) {
                game.createShape(pieces, name);
                game.addPlayerCreatedShapes();
            }
        }
    }.setWidth(150).setX(20).setY(60).setText("Create Shape").setFrameThickness(4).setScreen("PieceCreator");

    // The constructor creates the cells where the pieces can be placed on. This is a 5x5 area.
    public TetriminoCreatorMenu(GamePanel game) {
        super("PieceCreator", game);

        for (int x = -2; x <= 2; x++)
            for (int y = -2; y <= 2; y++)
                cells.add(new Cell(x, y));
    }

    // This renders the cells, the placed pieces, the example piece, the buttons and some labels for the buttons
    public void renderScreen(Graphics g) {
        for (Cell c : cells) {
            c.paint(g, cellPositionX, cellPositionY);
            g.setColor(Color.white);
            if(c.x == 0 && c.y == 0) g.drawString("0,0", cellPositionX + GlobalValues.size/2 - 8, cellPositionY + GlobalValues.size/2 + 5);
        }
        for (Piece p : pieces)
            p.paint(g, cellPositionX, cellPositionY);
        for(Button b:Button.buttons)b.render(g);
        GamePanel.drawStringWithShadow(g, "RED: " + red, 20, 20, 160, Color.darkGray, Color.white, false);
        GamePanel.drawStringWithShadow(g, "GREEN: " + green, 20, 20, 230, Color.darkGray, Color.white, false);
        GamePanel.drawStringWithShadow(g, "BLUE: " + blue, 20, 20, 300, Color.darkGray, Color.white, false);
        GamePanel.drawStringWithShadow(g, "TYPE: " + Character.toString(type.name().toLowerCase().charAt(0)).toUpperCase()+type.name().toLowerCase().substring(1), 20, 20, 370, Color.darkGray, Color.white, false);
        example_piece.setRGB(new Color(red, green, blue).getRGB());
        example_piece.paint(g, 170, 130);
    }

    // This function runs whenever you click on the screen. If you click inside a cell it will create a new Piece at that
    // location as long as there isn't one already there; if there is, it will remove it instead.
    public void clickedOnScreen(MouseEvent e) {
        if (e.getButton() == 1)
            for (Cell c : cells)
                if (e.getX() > (cellPositionX + c.x * GlobalValues.size + 5) && e.getX() < (GlobalValues.size + cellPositionX + c.x * GlobalValues.size + 5) && e.getY() > (cellPositionY + (c.y + 1) * GlobalValues.size) && e.getY() < (GlobalValues.size + cellPositionY + (c.y + 1) * GlobalValues.size)) {
                    Piece p = isPieceThere(c.x, c.y);
                    if (p == null) pieces.add(new Piece(type, new Color(red, green, blue).getRGB()).setX(c.x).setY(c.y));
                    else pieces.remove(p);
                }
        game.repaint();
    }

    // This is used to get the piece located at teh values x and y
    public Piece isPieceThere(int x, int y) {
        for (Piece p : pieces)
            if (p.getX() == x && p.getY() == y) return p;
        return null;
    }
}
