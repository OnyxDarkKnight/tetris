package gui.menus;

import game.GamePanel;
import gui.buttons.ButtonCustomGame;
import gui.buttons.ButtonExit;
import gui.buttons.ButtonPlay;
import gui.buttons.ButtonTetriminoCreator;
import utility.GlobalValues;

import java.awt.*;

/**
 * Created by siliut on 04/12/2015.
 * This simply displays the name of the game and the menu buttons, which are play, custom game (which is currently disabled), about, exit and the tetrimino cretor
 * which is only available in the app version
 */
public class MainMenu extends Menu
{
    gui.buttons.Button play = new ButtonPlay().setX((GlobalValues.gameWidth * GlobalValues.size) / 2).setY(200);
    gui.buttons.Button custom_game = new ButtonCustomGame().setFillCol(Color.gray).setX((GlobalValues.gameWidth * GlobalValues.size) / 2).setY(240).setEnabled(false);
    gui.buttons.Button about = new gui.buttons.Button(){
        public void onButtonClicked()
        {
            super.onButtonClicked();
            getGamePanel().screen = "About";
            getGamePanel().repaint();
        }
    }.setText("About").setFrameThickness(4).setScreen("MainMenu").setX((GlobalValues.gameWidth * GlobalValues.size) / 2).setY(280);
    gui.buttons.Button exit = new ButtonExit().setX((GlobalValues.gameWidth * GlobalValues.size) / 2).setY(320);
    gui.buttons.Button tetrimino_creator = new ButtonTetriminoCreator().setX((GlobalValues.gameWidth * GlobalValues.size) / 2).setY(360);

    public MainMenu()
    {
        super("MainMenu", null);
    }

    public void renderScreen(Graphics g)
    {
        GamePanel.drawStringWithShadow(g, "TETRIS RELOADED", 50, 20, 80, Color.blue, Color.yellow, false);
        for(gui.buttons.Button b : gui.buttons.Button.buttons) b.render(g);
    }
}
