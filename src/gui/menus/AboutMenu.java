package gui.menus;

import game.GamePanel;
import gui.buttons.ButtonMainMenu;

import java.awt.*;

/**
 * Created by Onyx on 07-Dec-15.
 * This is a menu that simply displays some information about the game
 */
public class AboutMenu extends Menu
{
    gui.buttons.Button menu = new ButtonMainMenu("About").setWidth(150).setX(20).setY(20);
    String[] info = {
            "Thank you for playing Tetris Reloaded, a game created by me,",
            "Sorin Iliuta, as part of my 2nd assignment for my",
            "Application Programming course. This was a really fun project that",
            "I plan to continue to work on as I still have some ideas of what to",
            "add in. Anyway, in case you are playing the applet version I have to",
            "mention there is also a jar version which contains and will contain",
            "some extra features such as: A Tetrimino Creator, Music, Challenges,",
            "Custom Games, Different Themes, special type of pieces and many more!","","",
            "This was made entirely in Java and the only external jars used are",
            "gson-2.5 by Google and json-simple-1.1.1 which are used to read",
            "and write json files. They both have an open license, meaning",
            "I am allowed to use them and even redistribute them. Below you",
            "can find links to their license pages.","",
            "Gson:", "https://github.com/google/gson/blob/master/LICENSE",
            "Json-simple:", "https://github.com/fangyidong/json-simple/blob/master/LICENSE.txt"
    };
    public AboutMenu()
    {
        super("About", null);
    }

    public void renderScreen(Graphics g)
    {
        menu.render(g);
        for(int i = 0; i < info.length; i++) GamePanel.drawStringWithShadow(g, info[i], 20, 350, 100 + i*20, Color.darkGray, Color.white, true);
    }
}
