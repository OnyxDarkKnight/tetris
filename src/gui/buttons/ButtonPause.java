package gui.buttons;

/**
 * Created by siliut on 04/12/2015.
 * This is a toggable button that is used to pause the game; its state is used to determine if the game is paused or not
 */
public class ButtonPause extends Button {
    public ButtonPause()
    {
        setText("Pause");
        setFrameThickness(4);
        setToggable(true);
        setScreen("Game");
    }

    public void onButtonClicked()
    {
        super.onButtonClicked();
        getGamePanel().repaint();
    }
}
