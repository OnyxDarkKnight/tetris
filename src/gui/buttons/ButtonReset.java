package gui.buttons;

import game.Cell;

/**
 * Created by siliut on 05/12/2015.
 * THis button only appears when the game is over and when clicked it reset the game
 */
public class ButtonReset extends Button
{
    public ButtonReset()
    {
        setText("Reset");
        setFrameThickness(4);
        setScreen("Game");
    }

    public void onButtonClicked()
    {
        super.onButtonClicked();
        getGamePanel().gameOver = false;
        getGamePanel().fallenShapes.removeAll(getGamePanel().fallenShapes);
        for (Cell c : getGamePanel().cells) c.isOccupied = false;
        setVisible(false);
        getGamePanel().exit.setVisible(false);
        getGamePanel().pause.setVisible(true);
        getGamePanel().score = 0;
        getGamePanel().repaint();
    }
}
