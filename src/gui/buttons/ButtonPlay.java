package gui.buttons;

/**
 * Created by siliut on 04/12/2015.
 * This button sets the screen to the Game screen and repaints the game afterwards
 */
public class ButtonPlay extends Button
{
    public ButtonPlay()
    {
        setText("Play");
        setFrameThickness(4);
        setScreen("MainMenu");
    }

    public void onButtonClicked()
    {
        super.onButtonClicked();
        getGamePanel().screen = "Game";
        getGamePanel().repaint();
    }
}
