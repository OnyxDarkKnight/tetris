package gui.buttons;

import game.GamePanel;
import game.Main;
import game.Tetris;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by siliut on 04/12/2015.
 * This is the main button class that contains all the methods and variables used by all buttons
 */
public class Button
{
    // Initializes default values for the coordinates of the button, the size of it, the thickness of the frame and text size
    public int x = 0, y = 0, width = 200, height = 30, frameThickness = 1, textSize = 20;
    // These variables are used for the actual text within the button and on what screen does this button belong to (used to
    // render the button on the proper screen and make sure if somebody clicks in the area it would exist nothing happens if not on the
    // proper screen)
    String text, screen;
    // These are some colors used for the button, such as the color of the frame, filling, the original color of the filling and the one when the button is clicked,
    // a color for the text shadow and one for the actual text
    Color frame = Color.blue, fill = Color.black, originalFill = Color.black, clickedFill = Color.blue.darker().darker(), textShadow = Color.darkGray, textCol = Color.white;
    // This is a variable used in case I want to add proper button images
    Image image;
    // THis is a list of all the buttons created, used in order to go through them and do certain actions (such as clicking, or releasing, or rendering)
    public static ArrayList<Button> buttons = new ArrayList<Button>();
    // Values used to determine if this is a toggable button, the state of it, if it's visible (if it is not, it won't render and no action will be done upon clicking)
    // or if it is enabled (unlcked visibility, this will always be rendered, but no action will be done upon clicking)
    boolean toggle, isToggable, isVisible = true, isEnabled = true;

    // Constructor to initialize the button with all the values inputed and then adds it to the list of buttons
    public Button(String text, Color textShadow, Color textCol, int textSize, int x, int y, int width, int height, int frameThickness, Color frame, Color fill)
    {
        setText(text);
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
        setShadowCol(textShadow);
        setTextCol(textCol);
        setTextSize(textSize);
        setFrameThickness(frameThickness);
        setFrameCol(frame);
        setFillCol(fill);
        buttons.add(this);
    }

    // A simple constructor that just adds it to the list of buttons
    public Button()
    {
        buttons.add(this);
    }

    // Function used to determine what happens when the button is clicked; by default it simply sets the filling color
    // to the original filling, or gray if it is not enabled
    public void onButtonClicked()
    {
        Color f = isEnabled?originalFill:Color.gray;
        fill = f;
        getGamePanel().repaint();
    }

    // Function used to determine what happens when the button is pressed; by default it simply sets the filling color
    // to the clicked filling value, or fark gray if it is not enabled
    public void onButtonPressed()
    {
        Color f = isEnabled?clickedFill:Color.darkGray;
        fill = f;
        getGamePanel().repaint();
    }

    // Function used to determine what happens when the button is released; by default it simply sets the filling color
    // to the original filling, or gray if it is not enabled (This is used to revert the filling color to the original
    // value, no matter where you release the button, whether it is inside the button or outside it)
    public void onButtonReleased()
    {
        Color f = isEnabled?originalFill:Color.gray;
        fill = f;
        getGamePanel().repaint();
    }

    // Function used to set the visibility; It returns Button so it can be used on the same line as the one it is created.
    // This is particularly useful if you don't want to set the value of every single variable and don't want to create
    // a million constructors to satisfy all possibilities
    public Button setVisible(boolean isVisible)
    {
        this.isVisible = isVisible;
        return this;
    }

    // Function used to set if it is enabled
    public Button setEnabled(boolean enabled)
    {
        this.isEnabled = enabled;
        return this;
    }

    // This returns the value of isVisible
    public boolean isVisible()
    {
        return isVisible;
    }
    // This return the value of isEnabled
    public boolean isEnabled()
    {
        return isEnabled;
    }

    /**
     * Beyond this point, until the render function, are simply functions used to set the value of each variable, such
     * as color, size, position, etc
     */
    public Button setX(int x)
    {
        this.x = x;
        return this;
    }

    public Button setY(int y)
    {
        this.y = y;
        return this;
    }

    public Button setWidth(int w)
    {
        this.width = w;
        return this;
    }

    public Button setHeight(int h)
    {
        this.height = h;
        return this;
    }

    public Button setTextSize(int textSize)
    {
        this.textSize = textSize;
        return this;
    }

    public Button setFrameThickness(int thickness)
    {
        this.frameThickness = thickness;
        return this;
    }

    public Button setText(String text)
    {
        this.text = text;
        return this;
    }

    public Button setScreen(String screen)
    {
        this.screen = screen;
        return this;
    }

    public Button setFrameCol(Color frameCol)
    {
        this.frame = frameCol;
        return this;
    }

    public Button setFillCol(Color fillCol)
    {
        this.fill = fillCol;
        originalFill = fillCol;
        return this;
    }

    public Button setClickedFillCol(Color fillCol)
    {
        this.clickedFill = fillCol;
        return this;
    }

    public Button setShadowCol(Color shadowCol)
    {
        this.textShadow = shadowCol;
        return this;
    }

    public Button setTextCol(Color textCol)
    {
        this.textCol = textCol;
        return this;
    }

    public Button setImage(Image image)
    {
        this.image = image;
        return this;
    }

    public boolean isToggable()
    {
        return this.isToggable;
    }

    public void setToggable(boolean t)
    {
        this.isToggable = t;
    }

    public boolean getState()
    {
        return toggle;
    }

    public void toggle()
    {
        this.toggle = !toggle;
    }

    public String getScreen()
    {
        return screen;
    }

    // This function is used to render the button; if there is an image it will use that instead of the rectangles. Also,
    // if it is disabled, it will display the word "Disabled" to the right of it
    public void render(Graphics g)
    {
        if(screen.equals(getGamePanel().screen) && isVisible) {
            Graphics2D g2 = (Graphics2D) g;
            if (image != null) g.drawImage(image, x, y, null);
            else {
                g2.setStroke(new BasicStroke(frameThickness));
                g.setColor(frame);
                g.drawRect(x, y, width, height);
                g.setColor(fill);
                g.fillRect(x, y, width, height);
            }
            GamePanel.drawStringWithShadow(g, text, textSize, x + width / 2, y + height / 2 + textSize / 2, textShadow, textCol, true);
            if(!isEnabled) GamePanel.drawStringWithShadow(g, "Disabled", textSize, x + width + 10, y + height / 2 + textSize / 2, textShadow, Color.red, false);
        }
    }

    // This function returns the according GamePanel, depending on whether this is an instance of the applet or application
    public GamePanel getGamePanel()
    {
        return Main.game == null? Tetris.game:Main.game;
    }
}
