package gui.buttons;

/**
 * Created by siliut on 05/12/2015.
 * This is a simple button tied to the MainMenu screen that, when clicked, it will set the screen to "CustomGame" and repaint the game
 */
public class ButtonCustomGame extends Button {
    public ButtonCustomGame()
    {
        setText("Custom Game");
        setFrameThickness(4);
        setScreen("MainMenu");
    }

    public void onButtonClicked()
    {
        super.onButtonClicked();
        getGamePanel().screen = "CustomGame";
        getGamePanel().repaint();
    }
}
