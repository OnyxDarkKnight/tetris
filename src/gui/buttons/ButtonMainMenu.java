package gui.buttons;

import game.Cell;

/**
 * Created by siliut on 05/12/2015.
 * This button Sets the screen to the Main Menu and if this button is located in the Game screen, then it will reset the board
 */
public class ButtonMainMenu extends Button
{
    public ButtonMainMenu(String screen)
    {
        setText("Main Menu");
        setFrameThickness(4);
        setScreen(screen);
    }

    public void onButtonClicked()
    {
        super.onButtonClicked();
        if(getScreen().equals("Game")) {
            getGamePanel().gameOver = false;
            getGamePanel().fallenShapes.removeAll(getGamePanel().fallenShapes);
            for (Cell c : getGamePanel().cells) c.isOccupied = false;
            getGamePanel().score = 0;
            if (getGamePanel().pause.getState()) getGamePanel().pause.toggle();
            getGamePanel().pause.setVisible(true);
            getGamePanel().addFallingShape();
        }
        getGamePanel().screen = "MainMenu";
        getGamePanel().repaint();
    }
}
