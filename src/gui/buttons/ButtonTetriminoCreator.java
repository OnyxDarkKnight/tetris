package gui.buttons;

/**
 * Created by siliut on 05/12/2015.
 * This button sets the screen to the Tetrimino Creator menu
 */
public class ButtonTetriminoCreator extends Button
{
    public ButtonTetriminoCreator()
    {
        setText("Tetrimino Creator");
        setFrameThickness(4);
        setScreen("MainMenu");
        if(getGamePanel().isJar) setVisible(true);
        else setVisible(false);
    }

    public void onButtonClicked()
    {
        super.onButtonClicked();
        getGamePanel().screen = "PieceCreator";
        getGamePanel().repaint();
    }
}
