package gui.buttons;

/**
 * Created by siliut on 05/12/2015.
 * This button simply closes the application
 */
public class ButtonExit extends Button {

    public ButtonExit()
    {
        setText("Exit");
        setFrameThickness(4);
        setScreen("MainMenu");
    }

    public void onButtonClicked()
    {
        super.onButtonClicked();
        System.exit(0);
    }
}
