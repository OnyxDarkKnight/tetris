package utility;

/**
 * Created by Onyx on 06-Dec-15.
 * This is an enum file used to determine the type of a piece. This is not very interesting now, but in the future
 * each piece will do something and look different depending on its type; This class also contains a next() and previous()
 * functions to make cycling between the enum values easy
 */
public enum EnumPieceType {
    NORMAL{
        @Override
        public EnumPieceType previous()
        {
            return values()[values().length - 1];
        }
    },
    EXPLOSSIVE,
    SAME_COLOR_REMOVAL,
    ROW_REMOVER,
    COLUMN_REMOVER,
    SHIELDED,
    ELECTRIC,
    BUFF,
    DOUBLE,
    POINTS_MULTIPLIER {
        @Override
        public EnumPieceType next()
        {
            return values()[0];
        }
    };

    public EnumPieceType next()
    {
        return values()[ordinal() + 1];
    }

    public EnumPieceType previous()
    {
        return values()[ordinal() - 1];
    }
}
