package utility;


import java.awt.*;

/**
 * Created by siliut on 16/10/2015.
 * This is simply a class containing some common used values
 */
public class GlobalValues {
    public static int size = 32;
    public static int gameWidth = 10;
    public static int gameHeight = 20;
    public static int infoWidth = 200;
    public static Image cell, piece;
}
