package utility;

import game.GamePanel;
import game.Tetris;
import shape.ObjectShape;
import shape.Piece;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by siliut on 20/11/2015.
 * This handles the timing of the game and the actions that should happen every certain amount of time
 */
public class TimerHandler implements ActionListener {
    GamePanel game;
    // This is used to determine whether a tetris piece can go down or not
    boolean canContinue = true;

    public TimerHandler(GamePanel game)
    {
        this.game = game;
    }

    // This method runs if the game is not over, he screen is the "Game" one and the game is not paused; it checks if the
    // shape can move down and then if it is game over (which, if it is, it sets it as that, enables the exit and reset
    // buttons and makes the pause button invisible, then repaints the screen). If it can move down, it increases the
    // Y position of the shape by the size of the piece (which is 32), otherwise it sets the tetrimino down,
    // sets the cell in which it lays as occupied, adds it to the fallen shapes list, sets the current falling shape to
    // the next one and assigns a new shape for the next value and calls the rowsFilled() function
    @Override
    public void actionPerformed(ActionEvent e) {
        if (!game.gameOver && game.screen.equals("Game") && !game.pause.getState()) {
            for (Piece p : shape().getPieces())
                if (!game.isEmpySpaceAt(shape().posX() + p.getX(), shape().posY() + p.getY() + 1) && shape().isOnScreen(game))
                    canContinue = false;
            for (Piece p : shape().getPieces())
                if (isGameOver(p)) {
                    canContinue = false;
                    game.gameOver = true;
                    game.exit.setVisible(true);
                    game.reset.setVisible(true);
                    game.pause.setVisible(false);
                    game.repaint();
                }

            if (canContinue) shape().setY(shape().y + GlobalValues.size);
            else {
                canContinue = true;
                game.fallenShapes.add(shape());
                for (Piece p : shape().getPieces())
                    game.setCellOccupied(shape().posX() + p.getX(), shape().posY() + p.getY());
                rowFilled();
                game.currentShape = game.nextShape;
                game.currentShape.setX((GlobalValues.gameWidth / 2) * GlobalValues.size).setY(-(game.currentShape.maxHeight + 1) * GlobalValues.size);
                game.nextShape = new ObjectShape().setX(20).setY(20);
                game.setShape(Tetris.rand.nextInt(game.shapes.size()), game.nextShape);
            }
            game.repaint();
        }
    }

    // This checks if the game is over
    boolean isGameOver(Piece p) {
        return (shape().isOffScreen(game) && p.getY() == shape().maxHeight && !game.isEmpySpaceAt(shape().posX() + p.getX(), shape().posY() + p.getY() + 1)) ||
                (shape().isPartiallyOnScreen(game) && p.getY() == shape().maxHeight && !game.isEmpySpaceAt(shape().posX() + p.getX(), shape().posY() + p.getY() + 1));
    }

    // This checks which rows have been filled, removes them and moves down the rest of the lines; it will also remove
    // any ObjectShape that has no pieces from the fallen shapes lists to conserve memory
    void rowFilled() {
        int firstRemovedRow = -1, emptyLines = 1, emptyCells;
        for (int i = 0; i < GlobalValues.gameHeight; i++)
            if (game.isRowFilled(i)) {
                for (ObjectShape s : game.fallenShapes) {
                    ArrayList<Piece> pieces = (ArrayList<Piece>) s.getPieces().clone();
                    for (Piece p : pieces)
                        if ((s.y + p.getY() * GlobalValues.size) == i * GlobalValues.size) {
                            game.setCellUnoccupied(s.x / GlobalValues.size + p.getX(), s.y / GlobalValues.size + p.getY());
                            s.removePiece(p);
                        }
                }
                if (i > firstRemovedRow) firstRemovedRow = i;
                game.score += 10;
            }

        if (firstRemovedRow >= 0) {
            for (int y = firstRemovedRow; y > 0; y--) {
                emptyCells = 0;
                for (int x = 0; x < GlobalValues.gameWidth; x++) {
                    if (game.isEmpySpaceAt(x, y) && !game.isEmpySpaceAt(x, y - 1)) {
                        game.setCellUnoccupied(x, y - 1);
                        Piece p = game.getPieceAt(x, y - 1);
                        p.setY(p.getY() + emptyLines);
                    } else emptyCells++;
                }
                if (emptyCells == GlobalValues.gameWidth) emptyLines++;
            }
        }

        ArrayList<ObjectShape> fallenPieces = (ArrayList<ObjectShape>) game.fallenShapes.clone();
        for (ObjectShape s : fallenPieces) {
            if (!s.getPieces().isEmpty()) for (Piece p : s.getPieces())
                game.setCellOccupied(s.x / GlobalValues.size + p.getX(), s.y / GlobalValues.size + p.getY());
            else game.fallenShapes.remove(s);
        }
    }

    // This is a basic function created just so I save space on the screen
    private ObjectShape shape() {
        return game.currentShape;
    }
}
