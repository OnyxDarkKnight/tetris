package utility;

import game.GamePanel;
import gui.buttons.Button;
import gui.menus.Menu;
import shape.ObjectShape;
import shape.Piece;

import java.awt.event.*;

/**
 * Created by siliut on 19/11/2015.
 * This handles the mouse input, key input and mouse wheel input
 */
public class InputHandler implements MouseWheelListener, MouseListener, KeyListener {
    GamePanel game;
    boolean canContinue = true;

    public InputHandler(GamePanel game) {
        this.game = game;
    }

    // Rotates the shape to the left or right (depending on the direction of the scroll wheel) if it can
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (!game.gameOver && game.screen.equals("Game") && !game.pause.getState()) {
            if (e.getWheelRotation() < 0 && game.currentShape.canRotateLeft(game)) {
                game.currentShape.rotateLeft();
                game.repaint();
            }
            if (e.getWheelRotation() > 0 && game.currentShape.canRotateRight(game)) {
                game.currentShape.rotateRight();
                game.repaint();
            }
        }
    }

    // First checks if you where you clicked is a button and if so does the action and makes sure no further actions
    // are activated; then it runs the clickedOnScreen method for each menu and then moves the shape left or right
    // (depending on what mouse button is pressed) if it can do so
    @Override
    public void mouseClicked(MouseEvent e) {
        int v = game.isJar ? 30 : 0;
        int u = game.isJar ? 10 : 0;
        for (Button b : Button.buttons)
            if (e.getX() > (u + b.x) && e.getX() < (u + b.x + b.width) && e.getY() > (b.y + v) && e.getY() < (b.y + b.height + v) && b.getScreen().equals(game.screen) && b.isVisible() && b.isEnabled()) {
                b.onButtonClicked();
                if (b.isToggable()) b.toggle();
                return;
            }

        for (Menu m : Menu.menus)
            if (game.screen.equals(m.name))
                m.clickedOnScreen(e);

        if (!game.gameOver && game.screen.equals("Game") && !game.pause.getState()) {
            if (e.getButton() == 1 && canMoveLeft(game.currentShape)) {
                game.currentShape.setX(game.currentShape.x - GlobalValues.size);
                game.repaint();
            }
            if (e.getButton() == 3 && canMoveRight(game.currentShape)) {
                game.currentShape.setX(game.currentShape.x + GlobalValues.size);
                game.repaint();
            }
        }
    }

    // It runs the on ButtonPressed() function for the button in that location you clicked (if there is one)
    @Override
    public void mousePressed(MouseEvent e) {
        int v = game.isJar ? 30 : 0;
        int u = game.isJar ? 10 : 0;
        for (Button b : Button.buttons)
            if (e.getX() > (u + b.x) && e.getX() < (u + b.x + b.width) && e.getY() > (b.y + v) && e.getY() < (b.y + b.height + v) && b.getScreen().equals(game.screen) && b.isVisible()) {
                b.onButtonPressed();
                return;
            }

    }

    // Runs the action of onButtonReleased for each one
    @Override
    public void mouseReleased(MouseEvent e) {
        for (Button b : Button.buttons) b.onButtonReleased();
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    // Moves the object left, right and down, depending on the key pressed and if the action is possible
    @Override
    public void keyPressed(KeyEvent e) {
        if (!game.gameOver && game.screen.equals("Game") && !game.pause.getState()) {
            if (e.getKeyCode() == e.VK_S || e.getKeyCode() == e.VK_DOWN) {
                canContinue = true;

                for (Piece p : shape().getPieces())
                    if (!game.isEmpySpaceAt(shape().posX() + p.getX(), shape().posY() + p.getY() + 1) && shape().isOnScreen(game))
                        canContinue = false;

                for (Piece p : shape().getPieces())
                    if (isGameOver(p)) {
                        canContinue = false;
                        game.gameOver = true;
                        game.exit.setVisible(true);
                        game.reset.setVisible(true);
                        game.pause.setVisible(false);
                        game.repaint();
                    }

                if (canContinue && !game.gameOver) {
                    shape().setY(shape().y + GlobalValues.size);
                    game.repaint();
                }
            }
            if ((e.getKeyCode() == e.VK_E || e.getKeyCode() == e.VK_UP) && game.currentShape.canRotateLeft(game)) {
                game.currentShape.rotateLeft();
                game.repaint();
            }
            if (e.getKeyCode() == e.VK_Q && game.currentShape.canRotateRight(game)) {
                game.currentShape.rotateRight();
                game.repaint();
            }
            if ((e.getKeyCode() == e.VK_A || e.getKeyCode() == e.VK_W || e.getKeyCode() == e.VK_LEFT) && canMoveLeft(game.currentShape)) {
                game.currentShape.setX(game.currentShape.x - GlobalValues.size);
                game.repaint();
            }
            if ((e.getKeyCode() == e.VK_D || e.getKeyCode() == e.VK_RIGHT) && canMoveRight(game.currentShape)) {
                game.currentShape.setX(game.currentShape.x + GlobalValues.size);
                game.repaint();
            }
        }
    }

    // Checks if the game is over
    boolean isGameOver(Piece p) {
        return (shape().isOffScreen(game) && p.getY() == shape().maxHeight && !game.isEmpySpaceAt(shape().posX() + p.getX(), shape().posY() + p.getY() + 1)) ||
                (shape().isPartiallyOnScreen(game) && p.getY() == shape().maxHeight && !game.isEmpySpaceAt(shape().posX() + p.getX(), shape().posY() + p.getY() + 1));
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    // Checks if you can move the object given to the right and returns true if it can
    public boolean canMoveRight(ObjectShape o) {
        boolean isAllOutside = false, isAllInside = false, isPartiallyInside = false;
        int inside = 0, outside = 0;

        for (Piece p : o.getPieces())
            if (o.y / GlobalValues.size + p.getY() < 0) outside++;
            else inside++;

        if (inside == 0 && outside > 0) isAllOutside = true;
        else if (inside > 0 && outside > 0) isPartiallyInside = true;
        else if (inside > 0 && outside == 0) isAllInside = true;

        if (isAllOutside) {
            for (Piece p : o.getPieces())
                if (o.x / GlobalValues.size + p.getX() >= GlobalValues.gameWidth - o.maxWidth - 1) return false;
        } else if (isPartiallyInside) {
            for (Piece p : o.getPieces())
                if (o.x / GlobalValues.size + p.getX() >= GlobalValues.gameWidth - o.maxWidth || o.y / GlobalValues.size + p.getY() >= 0 && !game.isEmpySpaceAt(o.x / GlobalValues.size + p.getX() + 1, o.y / GlobalValues.size + p.getY()))
                    return false;
        } else if (isAllInside) {
            for (Piece p : o.getPieces())
                if (!game.isEmpySpaceAt(o.x / GlobalValues.size + p.getX() + 1, o.y / GlobalValues.size + p.getY()))
                    return false;
        }
        return true;
    }

    // Checks if you can move the object given to the left and returns true if it can
    public boolean canMoveLeft(ObjectShape o) {
        boolean isAllOutside = false, isAllInside = false, isPartiallyInside = false;
        int inside = 0, outside = 0;

        for (Piece p : o.getPieces())
            if (o.y / GlobalValues.size + p.getY() < 0) outside++;
            else inside++;

        if (inside == 0 && outside > 0) isAllOutside = true;
        else if (inside > 0 && outside > 0) isPartiallyInside = true;
        else if (inside > 0 && outside == 0) isAllInside = true;

        if (isAllOutside) {
            for (Piece p : o.getPieces())
                if (o.x / GlobalValues.size + p.getX() <= o.maxWidth) return false;
        } else if (isPartiallyInside) {
            for (Piece p : o.getPieces())
                if (o.x / GlobalValues.size + p.getX() <= o.maxWidth || o.y / GlobalValues.size + p.getY() >= 0 && !game.isEmpySpaceAt(o.x / GlobalValues.size + p.getX() - 1, o.y / GlobalValues.size + p.getY()))
                    return false;
        } else if (isAllInside) {
            for (Piece p : o.getPieces())
                if (!game.isEmpySpaceAt(o.x / GlobalValues.size + p.getX() - 1, o.y / GlobalValues.size + p.getY()))
                    return false;
        }
        return true;
    }

    private ObjectShape shape() {
        return game.currentShape;
    }
}
