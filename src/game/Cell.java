package game;

import utility.GlobalValues;

import java.awt.*;

/**
 * Created by siliut on 16/10/2015
 * This is the Cell class. It is used to compose the actual grid of the game and to manage proper collisions.
 */
public class Cell {
    // Used to determine if this space is occupied by a piece of tetris or not
    public boolean isOccupied = false;
    // The coordinates of the cell
    public int x, y;

    // This is the constructor which requires a value for the coordinates when a new cell is created
    public Cell(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Draws the cell depending if the image was loaded or not
     * Because you can't properly read files in an applet, the cell is drawn as a simple square
     */
    public void paint(Graphics g)
    {
        paint(g, GlobalValues.infoWidth, 0);
    }

    // This is the same paint method, but this one you can reposition the cell on the screen
    public void paint(Graphics g, int posX, int posY)
    {
        if(GlobalValues.cell != null) {
            if (!isOccupied) g.drawImage(GlobalValues.cell, posX + x * GlobalValues.size, posY + y * GlobalValues.size, null);
        }
        else g.drawRect(posX + x * GlobalValues.size, posY + y * GlobalValues.size, GlobalValues.size, GlobalValues.size);
    }
}
