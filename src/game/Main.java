package game;

import gui.menus.AboutMenu;
import gui.menus.GameScreen;
import gui.menus.MainMenu;
import gui.menus.TetriminoCreatorMenu;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import utility.GlobalValues;
import utility.InputHandler;
import utility.TimerHandler;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.io.*;
import java.net.URL;

/**
 * Created by siliut on 26/11/2015.
 * This is the main file that starts for the Application part of the game
 */
public class Main extends JFrame {
    // These are just some values of the menus in the game (Main Menu, Tetrimino Creator, About Menu etc) and an input handler and a GamePanel
    public static GamePanel game;
    public static MainMenu menu;
    public static TetriminoCreatorMenu creatorMenu;
    public static GameScreen gameScreen;
    public static AboutMenu about;
    InputHandler input;

    // This initializes all the values and also tries to read the images; then it sets the size of the screen depending on the grid size,
    // creates a timer and starts it, then tries to play the music (This is exclusive to the Application)
    public Main() {
        super("Sorin Iliuta - 1401971");
        game = new GamePanel(true);
        gameScreen = new GameScreen(game);
        menu = new MainMenu();
        about = new AboutMenu();
        creatorMenu = new TetriminoCreatorMenu(game);
        try {
            GlobalValues.piece = ImageIO.read(getClass().getResource("/resources/piece.png"));
            GlobalValues.cell = ImageIO.read(getClass().getResource("/resources/cell.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setSize(GlobalValues.infoWidth + GlobalValues.gameWidth * (GlobalValues.size + 1), GlobalValues.gameHeight * (GlobalValues.size + 2));
        input = new InputHandler(game);
        add(game);
        addKeyListener(input);
        addMouseWheelListener(input);
        addMouseListener(input);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Timer time = new Timer(400, new TimerHandler(game));
        time.start();
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(getClass().getResource("/resources/tetris1.wav"));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.loop(Integer.MAX_VALUE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        setVisible(true);
    }

    // This is the main function that simply runs a new instance of this class
    public static void main(String[] args) {
        new Main();
    }
}
