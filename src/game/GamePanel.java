package game;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gui.buttons.*;
import gui.buttons.Button;
import gui.menus.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import shape.ObjectShape;
import shape.Piece;
import utility.EnumPieceType;
import utility.GlobalValues;

import javax.swing.*;
import java.awt.*;
import java.awt.Menu;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by siliut on 15/10/2015.
 * This is the class that initializes most things needed for the game
 */
public class GamePanel extends JPanel {
    // Stores all shapes that there are
    public ArrayList<ObjectShape> shapes = new ArrayList<ObjectShape>();
    // Stores all the fallen shapes
    public ArrayList<ObjectShape> fallenShapes = new ArrayList<ObjectShape>();
    // Stores the names of all player-created tetriminos files
    public ArrayList<String> foundFiles = new ArrayList<String>();
    //Stores all cells generated
    public ArrayList<Cell> cells = new ArrayList<Cell>();
    // These are the currently falling shape and the next shape about to fall
    public ObjectShape currentShape, nextShape;
    // Stores the value of the score
    public int score = 0;
    // gameOver is used to determine if a Game Over condition has been met, cellsEnabled is used to simply determine if cells are drawn or not and
    // isJar is used to determine if this runs as an applet or as an application
    public boolean gameOver = false, cellsEnabled = false, isJar = false;
    /*
        This is used to store the name of the screen (Example: it starts with "MainMenu" to show the main menu, then "Game" is for the actual game and so on).
        It serves the purpose of finding out what to render and if the buttons on the screen have any action on clicked, because each button is tied
        to a certain screen, to make sure screens don't clash
    */
    public String screen = "";
    /*
        These are the buttons that are contained in this screen; pause and menu are visible all the time, except pause, which is removed upon Game Over.
        Reset and exit appear only when the game is over and will disappear when the player starts a new game.
     */
    public static Button pause = new ButtonPause().setWidth(150).setX(20).setY(260);
    public static Button menu = new ButtonMainMenu("Game").setWidth(150).setX(20).setY(300);
    public static Button reset = new ButtonReset().setVisible(false).setWidth(150).setX(GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size / 2 - 75).setY(GlobalValues.gameHeight * GlobalValues.size / 2 + 20);
    public static Button exit = new ButtonExit().setVisible(false).setScreen("Game").setWidth(150).setX(GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size / 2 - 75).setY(GlobalValues.gameHeight * GlobalValues.size / 2 + 60);

    // The constructor sets the value of isJar accordingly, initializes the base pieces (see init() for more info), adds the cells to the screen
    // and then generates a falling shape and a next shape
    public GamePanel(boolean isJar) {
        this.isJar = isJar;
        screen = "MainMenu";
        init();
        initGameScreen();
        addFallingShape();
    }

    // Sets the background to black (which seems to be the color of every tetris game) and draws the menu that is set in the
    // screen variable
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.black);
        for(gui.menus.Menu m: gui.menus.Menu.menus)
            if(screen.equals(m.name))
                m.renderScreen(g);
    }

    // This is a simple function used for drawing strings with a shadow; It can also center the text.
    public static void drawStringWithShadow(Graphics g, String s, int size, int x, int y, Color col1, Color col2, boolean centered) {
        g.setFont(new Font("MSSerif", Font.PLAIN, size));
        g.setColor(col1);
        g.drawString(s, x - (centered ? g.getFontMetrics().stringWidth(s) / 2 : 0), y);
        g.setColor(col2);
        g.drawString(s, x - (centered ? g.getFontMetrics().stringWidth(s) / 2 : 0) + 1, y - 1);
    }

    // Method used to generate a falling shape and the next shape to fall
    public void addFallingShape() {
        currentShape = new ObjectShape();
        setShape(Tetris.rand.nextInt(shapes.size()), currentShape);
        currentShape.setX((GlobalValues.gameWidth / 2) * GlobalValues.size).setY(-(currentShape.maxHeight + 1) * GlobalValues.size);

        nextShape = new ObjectShape();
        setShape(Tetris.rand.nextInt(shapes.size()), nextShape);
        nextShape.setX(20).setY(20);
    }

    // Creates the grid by adding new cells to the cells list
    public void initGameScreen() {
        for (int x = 0; x < GlobalValues.gameWidth; x++)
            for (int y = 0; y < GlobalValues.gameHeight; y++)
                cells.add(new Cell(x, y));
    }

    // Method used to set the shape of an object based on a given type (the type being a number from 0 to the number of shapes in the list - 1)
    public void setShape(int type, ObjectShape shape) {
        shape.addPieces(shapes.get(type));
    }

    // Method used to determine if the cell at position x and y is not occupied by a Piece of a tetrimino
    public boolean isEmpySpaceAt(int x, int y) {
        for (Cell c : cells)
            if (c.x == x && c.y == y && !c.isOccupied) return true;
        return false;
    }

    // This is used to determine if there is a cell at the position x and y; it is useful to determine if an
    // object is out of the grid or not
    public boolean isCellAt(int x, int y) {
        for (Cell c : cells)
            if (c.x == x && c.y == y) return true;
        return false;
    }

    // This returns the piece of the Tetrimino located at x and y; It is used further to move the piece down when a row is filled
    public Piece getPieceAt(int x, int y) {
        for (ObjectShape s : fallenShapes)
            for (Piece p : s.getPieces())
                if ((s.x / GlobalValues.size + p.getX()) == x && (s.y / GlobalValues.size + p.getY()) == y)
                    return p;
        return null;
    }

    // This simply sets the cell at position x and y as occupied (meaning a Piece is now located in that spot); This uses
    // a lambda function; ALL HAIL THE LAMBDA FUNCTION \o/
    public void setCellOccupied(int x, int y) {
        for(Cell c : cells)
            if(c.x == x && c.y == y)
             c.isOccupied = true;
    }

    // This does the opposite of the previous function
    public void setCellUnoccupied(int x, int y) {
        for(Cell c : cells)
            if(c.x == x && c.y == y)
                c.isOccupied = false;
    }

    // This is used to determine if a row was filled or not by going through all the grid and counting how many empty spaces are on
    // a row and if that value matches the number of cells in the grid width, then clearly the row is filled
    public boolean isRowFilled(int y) {
        int i = 0;
        for (int x = 0; x < GlobalValues.gameWidth; x++)
            if (!isEmpySpaceAt(x, y)) i++;
        if (i == GlobalValues.gameWidth) return true;
        return false;
    }

    // This creates the basic shapes of the original tetris game and adds each one to the list of shapes;
    // After that, if this runs as an application, it generates an example json file in a new folder called "shapes".
    // This json file is an example of a custom made tetris shape that people can create. There is an in-game creator,
    // but this means people can share their shapes easily
    public void init() {
        ObjectShape s = new ObjectShape();
        s.addPiece(new Piece(0xFF66B2).setX(0).setY(0));
        s.addPiece(new Piece(0xFF66B2).setX(-1).setY(0));
        s.addPiece(new Piece(0xFF66B2).setX(0).setY(1));
        s.addPiece(new Piece(0xFF66B2).setX(0).setY(2));
        s.setBoundingBox();
        shapes.add(s);
        s = new ObjectShape();
        s.addPiece(new Piece(0xFF9933).setX(0).setY(0));
        s.addPiece(new Piece(0xFF9933).setX(1).setY(0));
        s.addPiece(new Piece(0xFF9933).setX(0).setY(1));
        s.addPiece(new Piece(0xFF9933).setX(0).setY(2));
        s.setBoundingBox();
        shapes.add(s);
        s = new ObjectShape();
        s.addPiece(new Piece(0xFFFF66).setX(0).setY(0));
        s.addPiece(new Piece(0xFFFF66).setX(1).setY(0));
        s.addPiece(new Piece(0xFFFF66).setX(0).setY(1));
        s.addPiece(new Piece(0xFFFF66).setX(1).setY(1));
        s.setBoundingBox();
        shapes.add(s);
        s.removePieces();
        s.addPiece(new Piece(0x33FFFF).setX(0).setY(0));
        s.addPiece(new Piece(0x33FFFF).setX(0).setY(1));
        s.addPiece(new Piece(0x33FFFF).setX(0).setY(2));
        s.addPiece(new Piece(0x33FFFF).setX(0).setY(3));
        s.setBoundingBox();
        shapes.add(s);
        s = new ObjectShape();
        s.addPiece(new Piece(0x990099).setX(0).setY(0));
        s.addPiece(new Piece(0x990099).setX(1).setY(0));
        s.addPiece(new Piece(0x990099).setX(-1).setY(0));
        s.addPiece(new Piece(0x990099).setX(0).setY(-1));
        s.setBoundingBox();
        shapes.add(s);
        s.removePieces();
        s.addPiece(new Piece(0xFF0000).setX(0).setY(0));
        s.addPiece(new Piece(0xFF0000).setX(-1).setY(0));
        s.addPiece(new Piece(0xFF0000).setX(0).setY(1));
        s.addPiece(new Piece(0xFF0000).setX(1).setY(1));
        s.setBoundingBox();
        shapes.add(s);
        s = new ObjectShape();
        s.addPiece(new Piece(0x66CC00).setX(0).setY(0));
        s.addPiece(new Piece(0x66CC00).setX(1).setY(0));
        s.addPiece(new Piece(0x66CC00).setX(0).setY(1));
        s.addPiece(new Piece(0x66CC00).setX(-1).setY(1));
        s.setBoundingBox();
        shapes.add(s);

        if (isJar) {
            try {
                File f = new File("shapes");
                if (!f.exists()) {
                    f.mkdirs();
                    File test_file = new File("shapes/shape_example.json");
                    test_file.createNewFile();
                    JSONObject shape = new JSONObject();
                    JSONArray shape_info = new JSONArray();
                    for (int x = 0; x < 2; x++) {
                        JSONObject piece = new JSONObject();
                        piece.put("x", x);
                        piece.put("y", 0);
                        piece.put("red", 70);
                        piece.put("green", 70);
                        piece.put("blue", 70);
                        piece.put("type", "NORMAL");
                        shape_info.add(piece);
                    }
                    shape.put("pieces", shape_info);
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    try (FileWriter file = new FileWriter(test_file)) {
                        file.write(gson.toJson(shape));
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // This is a function used to create a file of the shape given the pieces and the name of the shape; This, as well
    // as the previous method and the next, use Gson and json-simple library functions, which make it easy to read and
    // write json file; Gson is primarily used to write the json file in a more readable way
    public void createShape(ArrayList<Piece> pieces, String name) {
        try {
            File new_shape = new File("shapes/shape_" + name + ".json");
            new_shape.createNewFile();
            JSONObject shape = new JSONObject();
            JSONArray shape_info = new JSONArray();
            for (Piece p : pieces) {
                JSONObject piece = new JSONObject();
                piece.put("x", p.getX());
                piece.put("y", p.getY());
                piece.put("red", p.getColor().getRed());
                piece.put("green", p.getColor().getGreen());
                piece.put("blue", p.getColor().getBlue());
                piece.put("type", p.getType());
                shape_info.add(piece);
            }
            shape.put("pieces", shape_info);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            try (FileWriter file = new FileWriter(new_shape)) {
                file.write(gson.toJson(shape));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // This goes through all the files in the shapes folder and, as long as the file wasn't already read, it does that, adds
    // the shape to the list and stores the name of the file in the foundFiles list, to make sure there are no repeats
    public void addPlayerCreatedShapes() {
        try {
            JSONParser parser = new JSONParser();
            File f = new File("shapes");
            File[] matchingFiles = f.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.startsWith("shape_");
                }
            });
            for (int i = 0; i < matchingFiles.length && !foundFiles.contains(matchingFiles[i].toString()); i++) {
                foundFiles.add(matchingFiles[i].toString());
                ObjectShape s = new ObjectShape();
                Object obj = parser.parse(new FileReader(matchingFiles[i]));
                JSONObject jsonObject = (JSONObject) obj;
                JSONArray pieces = (JSONArray) jsonObject.get("pieces");
                Iterator it = pieces.iterator();
                while (it.hasNext()) {
                    JSONObject slide = (JSONObject) it.next();
                    int red = new Integer(slide.get("red").toString());
                    int green = new Integer(slide.get("green").toString());
                    int blue = new Integer(slide.get("blue").toString());
                    int x = new Integer(slide.get("x").toString());
                    int y = new Integer(slide.get("y").toString());
                    String type = (String) slide.get("type");
                    s.addPiece(new Piece(EnumPieceType.valueOf(type), new Color(red, green, blue).getRGB()).setX(x).setY(y));
                }
                shapes.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
