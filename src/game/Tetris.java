package game;

import gui.menus.AboutMenu;
import gui.menus.GameScreen;
import gui.menus.MainMenu;
import utility.GlobalValues;
import utility.InputHandler;
import utility.TimerHandler;

import javax.swing.*;
import java.util.Random;

/**
 * Created by siliut on 13/10/2015.
 * This is the main file that starts for the Applet part of the game
 */
public class Tetris extends JApplet {
    // These are just some values of the menus in the game (Main Menu, Tetrimino Creator, About Menu etc), an input handler, a GamePanel and a random variable
    public static GamePanel game;
    public static MainMenu menu;
    public static InputHandler input;
    public static Random rand = new Random();
    public static GameScreen gameScreen;
    public static AboutMenu about;

    // This initializes all the values, then it sets the size of the screen depending on the grid size,
    // adds the components to the applet (the game, the input handler which is a key listener, mouse listener and mouse wheel listener),
    // creates a timer and starts it, sets the screen as focusable and then focuses on it (this is to make sure that the applet
    // performs correctly with the inputs, without this I have discovered that the applet does not react to any kind of input given
    public void init() {
        super.init();
        game = new GamePanel(false);
        gameScreen = new GameScreen(game);
        menu = new MainMenu();
        about = new AboutMenu();
        setSize(GlobalValues.infoWidth + GlobalValues.gameWidth * GlobalValues.size, GlobalValues.gameHeight * GlobalValues.size);
        input = new InputHandler(game);
        add(game);
        addKeyListener(input);
        addMouseWheelListener(input);
        addMouseListener(input);
        Timer time = new Timer(400, new TimerHandler(game));
        time.start();
        setFocusable(true);
        requestFocusInWindow();
    }
}
